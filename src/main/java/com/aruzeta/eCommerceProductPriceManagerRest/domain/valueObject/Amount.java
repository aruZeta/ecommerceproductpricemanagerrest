package com.aruzeta.eCommerceProductPriceManagerRest.domain.valueObject;

import org.springframework.util.Assert;

public record Amount(
    double value
) {
    public Amount {
        Assert.isTrue(value > 0, "An amount of money can't be negative");
    }
}
