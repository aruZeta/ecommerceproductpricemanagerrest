package com.aruzeta.eCommerceProductPriceManagerRest.domain.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(EntityName entityName) {
        super(entityName.name + " not found");
    }

    public EntityNotFoundException(EntityName entityName, String message) {
        super(entityName.name + " not found for " + message);
    }

    @Getter
    @RequiredArgsConstructor
    public enum EntityName {
        FEE("Fee");

        private final String name;
    }
}
