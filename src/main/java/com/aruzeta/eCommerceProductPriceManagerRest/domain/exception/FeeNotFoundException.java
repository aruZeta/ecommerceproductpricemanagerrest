package com.aruzeta.eCommerceProductPriceManagerRest.domain.exception;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class FeeNotFoundException extends EntityNotFoundException {
    public FeeNotFoundException(long brandId, long productId, OffsetDateTime date) {
        super(EntityName.FEE, String.format(
            "brandId = %d, productId = %d and date = %s",
            brandId, productId, date.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
        ));
    }
}
