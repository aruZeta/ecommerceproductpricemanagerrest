package com.aruzeta.eCommerceProductPriceManagerRest.domain.service;

import com.aruzeta.eCommerceProductPriceManagerRest.domain.aggregate.Fee;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.exception.FeeNotFoundException;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.repository.IFeeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;
import java.util.Comparator;

public class FeeService {
    private final IFeeRepository feeRepository;

    @Autowired
    public FeeService(IFeeRepository feeRepository) {
        this.feeRepository = feeRepository;
    }

    public Fee searchByDate(OffsetDateTime date, long brand, long product) {
        return feeRepository
            .findByBrandAndProduct(brand, product)
            .filter(fee -> fee.getStartDate().isBefore(date) && fee.getEndDate().isAfter(date))
            .max(Comparator.comparingInt(Fee::getPriority))
            .orElseThrow(() -> new FeeNotFoundException(brand, product, date));
    }
}
