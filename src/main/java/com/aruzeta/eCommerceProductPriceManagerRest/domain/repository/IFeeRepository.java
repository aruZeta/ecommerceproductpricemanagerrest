package com.aruzeta.eCommerceProductPriceManagerRest.domain.repository;

import com.aruzeta.eCommerceProductPriceManagerRest.domain.aggregate.Fee;

import java.util.stream.Stream;

public interface IFeeRepository {
    Stream<Fee> findByBrandAndProduct(long brand, long product);
}
