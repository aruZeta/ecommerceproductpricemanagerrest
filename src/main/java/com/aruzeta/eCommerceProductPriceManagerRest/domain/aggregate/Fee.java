package com.aruzeta.eCommerceProductPriceManagerRest.domain.aggregate;

import com.aruzeta.eCommerceProductPriceManagerRest.domain.valueObject.Amount;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.valueObject.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.OffsetDateTime;

@Getter
@Builder
@AllArgsConstructor
public class Fee {
    private long id;
    private long brand;
    private long product;

    private OffsetDateTime startDate;
    private OffsetDateTime endDate;

    private int priority;
    private Amount price;
    private Currency currency;

    private OffsetDateTime lastUpdate;
    private String lastUpdatedBy;
}
