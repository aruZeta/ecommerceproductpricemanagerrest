package com.aruzeta.eCommerceProductPriceManagerRest;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.repository.config.AbstractJdbcConfiguration;
import org.springframework.data.relational.core.dialect.Dialect;
import org.springframework.data.relational.core.dialect.H2Dialect;
import org.springframework.data.relational.core.dialect.PostgresDialect;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

@Configuration
public class SpringDataJdbcConfiguration extends AbstractJdbcConfiguration {
    @Override
    public Dialect jdbcDialect(NamedParameterJdbcOperations operations) {
        return operations.getJdbcOperations().execute(
            (ConnectionCallback<Dialect>) connection ->
                switch (connection.getMetaData().getDatabaseProductName().toUpperCase()) {
                    case "H2" -> H2Dialect.INSTANCE;
                    case "POSTGRESQL" -> PostgresDialect.INSTANCE;
                    // ...
                    default -> super.jdbcDialect(operations);
                }
        );
    }
}