package com.aruzeta.eCommerceProductPriceManagerRest.application.dto;

import lombok.Builder;
import lombok.Getter;

import java.time.OffsetDateTime;

@Builder
@Getter
public class SearchFeeByDateRequest {
    private OffsetDateTime date;
    private long brand;
    private long product;
}