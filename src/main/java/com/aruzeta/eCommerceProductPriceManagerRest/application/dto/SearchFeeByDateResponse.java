package com.aruzeta.eCommerceProductPriceManagerRest.application.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class SearchFeeByDateResponse {
    private long fee;
    private long brand;
    private long product;

    private String startDate;
    private String endDate;

    private double price;
    private String currency;
}