package com.aruzeta.eCommerceProductPriceManagerRest.application.mapper;

import com.aruzeta.eCommerceProductPriceManagerRest.application.dto.SearchFeeByDateResponse;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.aggregate.Fee;

public class SearchFeeByDateResponseMapper {
    public static SearchFeeByDateResponse toResponse(Fee fee) {
        return SearchFeeByDateResponse.builder()
            .fee(fee.getId())
            .product(fee.getProduct())
            .brand(fee.getBrand())
            .startDate(fee.getStartDate().toString())
            .endDate(fee.getEndDate().toString())
            .price(fee.getPrice().value())
            .currency(fee.getCurrency().name())
            .build();
    }
}
