package com.aruzeta.eCommerceProductPriceManagerRest.application;

import com.aruzeta.eCommerceProductPriceManagerRest.application.dto.SearchFeeByDateRequest;
import com.aruzeta.eCommerceProductPriceManagerRest.application.dto.SearchFeeByDateResponse;
import com.aruzeta.eCommerceProductPriceManagerRest.application.mapper.SearchFeeByDateResponseMapper;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.aggregate.Fee;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.service.FeeService;
import org.springframework.beans.factory.annotation.Autowired;

public class SearchFeeByDateUseCase {
    private final FeeService feeService;

    @Autowired
    public SearchFeeByDateUseCase(FeeService feeService) {
        this.feeService = feeService;
    }

    public SearchFeeByDateResponse invoke(SearchFeeByDateRequest request) {
        Fee fee = feeService.searchByDate(request.getDate(), request.getBrand(), request.getProduct());
        return SearchFeeByDateResponseMapper.toResponse(fee);
    }
}
