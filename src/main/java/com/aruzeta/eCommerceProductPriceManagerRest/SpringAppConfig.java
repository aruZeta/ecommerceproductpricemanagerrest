package com.aruzeta.eCommerceProductPriceManagerRest;

import com.aruzeta.eCommerceProductPriceManagerRest.application.SearchFeeByDateUseCase;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.repository.IFeeRepository;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.service.FeeService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringAppConfig {
    @Bean
    public FeeService provideFeeService(IFeeRepository feeRepository) {
        return new FeeService(feeRepository);
    }

    @Bean
    public SearchFeeByDateUseCase provideSearchFeeByDateUseCase(FeeService feeService) {
        return new SearchFeeByDateUseCase(feeService);
    }
}
