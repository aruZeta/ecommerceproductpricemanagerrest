package com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.controller;

import com.aruzeta.eCommerceProductPriceManagerRest.application.dto.SearchFeeByDateRequest;
import com.aruzeta.eCommerceProductPriceManagerRest.application.dto.SearchFeeByDateResponse;
import com.aruzeta.eCommerceProductPriceManagerRest.application.SearchFeeByDateUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.OffsetDateTime;

@RestController
public class FeeController {
    private final SearchFeeByDateUseCase searchFeeByDateUseCase;

    @Autowired
    public FeeController(SearchFeeByDateUseCase searchFeeByDateUseCase) {
        this.searchFeeByDateUseCase = searchFeeByDateUseCase;
    }

    @GetMapping("/brands/{brand}/products/{product}/fees")
    public SearchFeeByDateResponse getFeeByDate(
        @PathVariable("brand") long brand,
        @PathVariable("product") long product,
        @RequestParam("date") String date
    ) {
        SearchFeeByDateRequest request = SearchFeeByDateRequest.builder()
            .date(OffsetDateTime.parse(date))
            .brand(brand)
            .product(product)
            .build();

        return searchFeeByDateUseCase.invoke(request);
    }
}
