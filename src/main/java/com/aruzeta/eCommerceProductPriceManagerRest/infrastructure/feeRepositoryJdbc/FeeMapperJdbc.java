package com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.feeRepositoryJdbc;

import com.aruzeta.eCommerceProductPriceManagerRest.domain.aggregate.Fee;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.valueObject.Amount;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.valueObject.Currency;
import com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.feeRepositoryJdbc.FeeJdbc;

public class FeeMapperJdbc {
    public static Fee toDomainModel(FeeJdbc feeJdbc) {
        return Fee.builder()
            .id(feeJdbc.getId())
            .brand(feeJdbc.getBrand())
            .product(feeJdbc.getProduct())
            .startDate(feeJdbc.getStartDate())
            .endDate(feeJdbc.getEndDate())
            .priority(feeJdbc.getPriority())
            .price(new Amount(feeJdbc.getPrice()))
            .currency(Currency.valueOf(feeJdbc.getCurrency()))
            .lastUpdate(feeJdbc.getLastUpdate())
            .lastUpdatedBy(feeJdbc.getLastUpdatedBy())
            .build();
    }
}
