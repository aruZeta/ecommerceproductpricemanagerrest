package com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.feeRepositoryJdbc;

import org.springframework.data.repository.Repository;

import java.util.List;

public interface FeeRepositoryJdbc extends Repository<FeeJdbc, Long> {
    List<FeeJdbc> findByBrandAndProduct(long brand, long product);
    boolean existsByBrand(long brand);
    boolean existsByProductAndBrand(long product, long brand);
}
