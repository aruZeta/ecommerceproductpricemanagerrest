package com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.feeRepositoryJdbc;

import com.aruzeta.eCommerceProductPriceManagerRest.domain.aggregate.Fee;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.repository.IFeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public class FeeRepositoryAdapterJdbc implements IFeeRepository {
    private final FeeRepositoryJdbc repository;

    @Autowired
    public FeeRepositoryAdapterJdbc(FeeRepositoryJdbc repository) {
        this.repository = repository;
    }

    @Override
    public Stream<Fee> findByBrandAndProduct(long brand, long product) {
        return repository
            .findByBrandAndProduct(brand, product)
            .stream()
            .map(FeeMapperJdbc::toDomainModel);
    }
}
