package com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.feeRepositoryJdbc;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table("FEE")
public class FeeJdbc {
    @Id
    @Column("ID")
    private long id;
    @Column("BRAND")
    private long brand;
    @Column("PRODUCT")
    private long product;

    @Column("START_DATE")
    private OffsetDateTime startDate;
    @Column("END_DATE")
    private OffsetDateTime endDate;

    @Column("PRIORITY")
    private int priority;
    @Column("PRICE")
    private double price;
    @Column("CURRENCY")
    private String currency;

    @Column("LAST_UPDATE")
    private OffsetDateTime lastUpdate;
    @Column("LAST_UPDATED_BY")
    private String lastUpdatedBy;
}
