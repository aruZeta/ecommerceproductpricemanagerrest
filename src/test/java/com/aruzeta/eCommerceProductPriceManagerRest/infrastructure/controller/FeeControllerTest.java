package com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.controller;

import com.aruzeta.eCommerceProductPriceManagerRest.SpringApp;
import com.aruzeta.eCommerceProductPriceManagerRest.SpringAppConfig;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
    classes = SpringApp.class
)
@ContextConfiguration(classes = SpringAppConfig.class)
@AutoConfigureMockMvc
class FeeControllerTest {
    private final MockMvc mvc;

    @Autowired
    public FeeControllerTest(MockMvc mvc) {
        this.mvc = mvc;
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sampleFeeQueriesWithResult.csv")
    public void testFeeSearchesByDateRest(String date, long brand, long product, long expectedId, double expectedPrice) throws Exception {
        // Act
        ResultActions resultActions = mvc.perform(
            get("/brands/{brand}/products/{product}/fees?date={date}", brand, product, date)
        );

        // Assert
        resultActions
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("fee").value(expectedId))
            .andExpect(jsonPath("price").value(expectedPrice));
    }

    @ParameterizedTest
    @CsvFileSource(resources = {
        "/sampleNonExistentBrandRequests.csv",
        "/sampleNonExistentProductRequests.csv",
        "/sampleFeeQueriesWithNoResults.csv"
    })
    public void testNotFoundQueries(String date, long brand, long product) throws Exception {
        // Act
        ResultActions resultActions = mvc.perform(
            get("/brands/{brand}/products/{product}/fees?date={date}", brand, product, date)
        );

        // Assert
        resultActions
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
            .andExpect(content().string("Fee not found for brandId = " + brand + ", productId = " + product + " and date = " + date));
    }

    @ParameterizedTest
    @CsvFileSource(resources = {
        "/sampleWrongFormattedDateRequests.csv"
    })
    public void testBadRequests(String date, long brand, long product) throws Exception {
        // Act
        ResultActions resultActions = mvc.perform(
            get("/brands/{brand}/products/{product}/fees?date={date}", brand, product, date)
        );

        // Assert
        resultActions
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN));
    }
}