package com.aruzeta.eCommerceProductPriceManagerRest.application;

import com.aruzeta.eCommerceProductPriceManagerRest.application.dto.SearchFeeByDateRequest;
import com.aruzeta.eCommerceProductPriceManagerRest.application.dto.SearchFeeByDateResponse;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.aggregate.Fee;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.exception.FeeNotFoundException;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.repository.IFeeRepository;
import com.aruzeta.eCommerceProductPriceManagerRest.domain.service.FeeService;

import static org.assertj.core.api.Assertions.*;

import static org.mockito.Mockito.*;

import com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.feeRepositoryJdbc.FeeJdbc;
import com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.feeRepositoryJdbc.FeeMapperJdbc;
import com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.feeRepositoryJdbc.FeeRepositoryJdbc;
import com.aruzeta.eCommerceProductPriceManagerRest.infrastructure.feeRepositoryJdbc.FeeRepositoryAdapterJdbc;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.stream.Stream;

public class SearchFeeByDateUseCaseTest {
    private final FeeRepositoryJdbc feeRepositoryJdbc = mock();
    private final IFeeRepository feeRepository = new FeeRepositoryAdapterJdbc(feeRepositoryJdbc);
    private final FeeService feeService = new FeeService(feeRepository);
    private final SearchFeeByDateUseCase useCase = new SearchFeeByDateUseCase(feeService);

    @ParameterizedTest
    @CsvFileSource(resources = "/sampleFeeQueriesWithResult.csv")
    public void testFeeSearchesByDate(String date, long brand, long product, long expectedId) {
        // Arrange
        when(feeRepositoryJdbc.findByBrandAndProduct(brand, product)).thenReturn(
            sampleFees().filter(fee -> fee.getBrand() == brand && fee.getProduct() == product).toList()
        );

        SearchFeeByDateRequest request = SearchFeeByDateRequest.builder()
            .date(OffsetDateTime.parse(date))
            .brand(brand)
            .product(product)
            .build();

        Fee expectedFee = getByIdFromSamples(expectedId);

        // Act
        SearchFeeByDateResponse response = useCase.invoke(request);

        // Assert
        assertThat(response.getFee()).isEqualTo(expectedFee.getId());
        assertThat(response.getBrand()).isEqualTo(expectedFee.getBrand());
        assertThat(response.getProduct()).isEqualTo(expectedFee.getProduct());
        assertThat(response.getStartDate()).isEqualTo(expectedFee.getStartDate().toString());
        assertThat(response.getEndDate()).isEqualTo(expectedFee.getEndDate().toString());
        assertThat(response.getPrice()).isEqualTo(expectedFee.getPrice().value());
        assertThat(response.getCurrency()).isEqualTo(expectedFee.getCurrency().name());
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sampleNonExistentBrandRequests.csv")
    public void testNonExistentBrands(String date, long brand, long product) {
        // Arrange
        when(feeRepositoryJdbc.findByBrandAndProduct(brand, product)).thenReturn(Collections.emptyList());
        when(feeRepositoryJdbc.existsByBrand(brand)).thenReturn(false);

        SearchFeeByDateRequest request = SearchFeeByDateRequest.builder()
            .date(OffsetDateTime.parse(date))
            .brand(brand)
            .product(product)
            .build();

        // Act & Assert
        assertThatThrownBy(() -> useCase.invoke(request))
            .isExactlyInstanceOf(FeeNotFoundException.class)
            .hasMessage("Fee not found for brandId = " + brand + ", productId = " + product + " and date = " + date);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sampleNonExistentProductRequests.csv")
    public void testNonExistentProducts(String date, long brand, long product) {
        // Arrange
        when(feeRepositoryJdbc.findByBrandAndProduct(brand, product)).thenReturn(Collections.emptyList());
        when(feeRepositoryJdbc.existsByBrand(brand)).thenReturn(true);
        when(feeRepositoryJdbc.existsByProductAndBrand(product, brand)).thenReturn(false);

        SearchFeeByDateRequest request = SearchFeeByDateRequest.builder()
            .date(OffsetDateTime.parse(date))
            .brand(brand)
            .product(product)
            .build();

        // Act & Assert
        assertThatThrownBy(() -> useCase.invoke(request))
            .isExactlyInstanceOf(FeeNotFoundException.class)
            .hasMessage("Fee not found for brandId = " + brand + ", productId = " + product + " and date = " + date);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/sampleFeeQueriesWithNoResults.csv")
    public void testNoResultsInDate(String date, long brand, long product) {
        // Arrange
        when(feeRepositoryJdbc.findByBrandAndProduct(brand, product)).thenReturn(Collections.emptyList());
        when(feeRepositoryJdbc.existsByBrand(brand)).thenReturn(true);
        when(feeRepositoryJdbc.existsByProductAndBrand(product, brand)).thenReturn(true);

        SearchFeeByDateRequest request = SearchFeeByDateRequest.builder()
            .date(OffsetDateTime.parse(date))
            .brand(brand)
            .product(product)
            .build();

        // Act & Assert
        assertThatThrownBy(() -> useCase.invoke(request))
            .isExactlyInstanceOf(FeeNotFoundException.class)
            .hasMessage("Fee not found for brandId = " + brand + ", productId = " + product + " and date = " + date);
    }

    public static Stream<FeeJdbc> sampleFees() {
        return Stream.of(
            new FeeJdbc(1, 1, 35455, OffsetDateTime.parse("2020-06-14T00:00:00Z"), OffsetDateTime.parse("2020-12-31T23:59:59Z"), 0, 35.50, "EUR", OffsetDateTime.parse("2020-03-26T14:49:07Z"), "user1"),
            new FeeJdbc(2, 1, 35455, OffsetDateTime.parse("2020-06-14T15:00:00Z"), OffsetDateTime.parse("2020-06-14T18:30:00Z"), 1, 25.45, "EUR", OffsetDateTime.parse("2020-05-26T15:38:22Z"), "user1"),
            new FeeJdbc(3, 1, 35455, OffsetDateTime.parse("2020-06-15T00:00:00Z"), OffsetDateTime.parse("2020-06-15T11:00:00Z"), 1, 30.50, "EUR", OffsetDateTime.parse("2020-05-26T15:39:22Z"), "user2"),
            new FeeJdbc(4, 1, 35455, OffsetDateTime.parse("2020-06-15T16:00:00Z"), OffsetDateTime.parse("2020-12-31T23:59:59Z"), 1, 38.95, "EUR", OffsetDateTime.parse("2020-06-02T10:14:00Z"), "user1")
        );
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    public static Fee getByIdFromSamples(long id) {
        return FeeMapperJdbc.toDomainModel(sampleFees().filter(fee -> fee.getId() == id).findFirst().get());
    }
}
