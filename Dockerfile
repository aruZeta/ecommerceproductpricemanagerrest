FROM maven:3-openjdk-17 AS builder
COPY . /app
RUN mvn -f /app/pom.xml package

FROM openjdk:17-alpine
COPY --from=builder /app/target/ECommerceProductPriceManagerRest-jar-with-dependencies.jar /app/main.jar
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "/app/main.jar" ]
