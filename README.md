## Introduction

This project provides a REST API to query the fee to apply to a
product depending on the passed date, based on the data saved in a
database.

This is quite useful for managing prices in sales at determined dates
like christmas.

## How it works

An entry on the database looks like:
```
id brand product start_date           end_date             priority price currency last_update          last_updated_by
1  1     35455   2020-06-14T00:00:00Z 2020-12-31T23:59:59Z 0        35.50 EUR      2020-03-26T14:49:07Z user1
```

Each entry is a fee with a price and a currency for a product and it's
brand, the fee lasts from `start_date` to `end_date` and, if any other
fees to the same product and brand match, the one with the highest
priority is the one to be used.

# Usage

First of all, start the server!

```bash
mvn spring-boot:run
```

And now you are all set to make a GET request to
`<server_url>/brands/<brand>/products/<product>/fees?date=<date>`
and you will get a JSON response with all the data you need:
```json
{
  "fee": <fee_id>,
  "brand": <brand_id>,
  "product": <product_id>,
  "startDate": "<date>",
  "endDate": "<date>",
  "price": <price>,
  "currency": "<currency>"
}
```

## About the project

- The project is written in Java using Spring Boot.
- Uses the TDD approach.
- Follows clean architectures like DDD, Ports & Adapters and CQS.
- Conventional commits with good branching practices.
